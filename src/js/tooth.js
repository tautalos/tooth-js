
;var Tooth = ( function(window){

    var Tooth,
         _Private = {};

    _Private.convertIDtoPropertyName = function convertIDtoPropertyName(name){

        var index,
            splitedStringArray,
            propertyName;

        if( name.indexOf('-') === -1 && name.indexOf('_') === -1 ){
            return name;
        }
        else{
            splitedStringArray = name.split(/\-|\_/); // replace - and _
            propertyName = splitedStringArray[0];
            for( index = 1; index < splitedStringArray.length; index += 1 ){
                propertyName +=
                    splitedStringArray[index].charAt(0).toUpperCase() +
                    splitedStringArray[index].substring(1, splitedStringArray[index].length);
            }
            return propertyName;
        }
    };

    var Tooth = function Tooth(){
        var Tooth = this;
        Tooth.ready = false;

        /**
         * doc is the flat html document representation of the
         * elements with an [id] attribute in the dom when loaded
         * or after the Tooth update function is called.
         * @type {{}}
         */
        Tooth.doc = {};

        /**
         * function to be called when the event "DOMContentLoaded"
         * is triggered.
         */
        Tooth.init = function init(){
            Tooth.update();
        }

        /**
         * the update function itereates over the dom and recreates
         * Tooth's doc object.
         */
        Tooth.update = function update(){
            Tooth.ready = false;
            Tooth.doc = {};
            var tempName,
                identified = document.querySelectorAll('[id]');

            for( var index = 0; index < identified.length; index += 1){
                tempName = _Private.convertIDtoPropertyName( identified[index].id );
                Tooth.doc[tempName] = identified[index];
            }
            window.ID = Tooth.doc;
            Tooth.ready = true;
        }
    };

    Tooth = new Tooth();
    document.addEventListener("DOMContentLoaded", function (event) {
        Tooth.init();
    });

    return Tooth;
}(window));




