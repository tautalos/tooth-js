describe('Tooth', function () {

    //if( isBrowser ){
    //
    //    it('should foo', function () {
    //        // This test will only run in Browsers
    //        expect( isBrowser).to.be.ok;
    //    });
    //}
    //else{
    //
    //    it('should bar', function () {
    //       // This test will only run in node
    //        expect( isBrowser ).not.to.be.ok;
    //    });
    //}

    it('should be defined', function (done) {

        expect( Tooth ).to.be.ok;
        expect( Tooth.ready ).to.be.ok;
        done();
    });

    it('.doc: should be defined', function () {
        expect( Tooth.doc ).to.be.ok;
    });

    it('update(): should recognize new element with [id] attribute added to the dom', function () {
        var element = document.createElement('div');
        element.id = "one";
        document.body.appendChild(element);
        Tooth.update();
        expect(Tooth.doc.one).to.equal( element );
    });
})