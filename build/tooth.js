/*

 Tooth JS:
     version: 0.0.0
     date: Sun Jan 11 2015 19:22:46 GMT+0000 (GMT)
     author: Tautalos
     url: http://www.kalibuda.com
     github: https://github.com/tautalos
     description:
        Tooth JS is a tiny library that provides easy access to html elements
        that have an [id] attribute at the event of DOM load. From there onwards,
        the user is responsible for managing the Tooth.doc object. This is, new
        elements with an [id] should added to the Tooth.doc object or inefficiently,
        the Tooth.update() function called to recapture all element currently in
        the dom with an [id].
     license: MIT
     licenseUrl: http://opensource.org/licenses/MIT
 */

;var Tooth = ( function(){

    var Tooth,
         _Private = {};

    _Private.convertIDtoPropertyName = function convertIDtoPropertyName(name){

        var index,
            splitedStringArray,
            propertyName;

        if( name.indexOf('-') === -1 && name.indexOf('_') === -1 ){
            return name;
        }
        else{
            splitedStringArray = name.split(/\-|\_/); // replace - and _
            propertyName = splitedStringArray[0];
            for( index = 1; index < splitedStringArray.length; index += 1 ){
                propertyName +=
                    splitedStringArray[index].charAt(0).toUpperCase() +
                    splitedStringArray[index].substring(1, splitedStringArray[index].length);
            }
            return propertyName;
        }
    };

    var Tooth = function Tooth(){
        var Tooth = this;
        Tooth.ready = false;

        /**
         * doc is the flat html document representation of the
         * elements with an [id] attribute in the dom when loaded
         * or after the Tooth update function is called.
         * @type {{}}
         */
        Tooth.doc = {};

        /**
         * function to be called when the event "DOMContentLoaded"
         * is triggered.
         */
        Tooth.init = function init(){
            Tooth.update();
        }

        /**
         * the update function itereates over the dom and recreates
         * Tooth's doc object.
         */
        Tooth.update = function update(){
            Tooth.ready = false;
            Tooth.doc = {};
            var tempName,
                identified = document.querySelectorAll('[id]');

            for( var index = 0; index < identified.length; index += 1){
                tempName = _Private.convertIDtoPropertyName( identified[index].id );
                Tooth.doc[tempName] = identified[index];
            }
            Tooth.ready = true;
        }
    };

    Tooth = new Tooth();
    document.addEventListener("DOMContentLoaded", function (event) {
        Tooth.init();
    });

    return Tooth;
}());
var ID = Tooth.doc;



