//------------------------------------------------------
// GULP CONFIG
//------------------------------------------------------

// Paths
//
var paths    = {};
paths.base     = {
    path    : './'
};
// src
// -----------------------------
paths.src       = {
    path: paths.base.path + 'src/',
}
paths.src.js    = {
    path: paths.src.path + 'js/'
}
paths.src.docs  = {
    path: paths.src.path + 'docs/'
}
// build
// -----------------------------
paths.build     = {
    path: paths.base.path + 'build/',
}
paths.build.js  = {
    path: paths.build.path + ''
}
paths.build.docs= {
    path: paths.build.path + 'docs/'
}
// Test
// -----------------------------
paths.test      = {
    path: paths.base.path +'test/'
}

//------------------------------------------------------
// GULP: Requires
//------------------------------------------------------

var gulp            = require( 'gulp' ),
    stripDebug      = require( 'gulp-strip-debug' ),
    header          = require( 'gulp-header' ),
    uglify          = require( 'gulp-uglify' ),
    concat          = require( 'gulp-concat' ),
    fileSystem      = require( 'fs' ),
    browserSync     = require( 'browser-sync' ),
    del             = require( 'del' );

//------------------------------------------------------
// Utilities & Functions
//------------------------------------------------------

// Get version using NodeJs filesystem
//
var getVersion = function(){
    return fileSystem.readFileSync('Version');
}

// Get copyright using NodeJs filesystem
//
var getCopyright = function(){
    return fileSystem.readFileSync('Copyright');
}

var getDate = function(){
    return new Date();
}


// Sync with browser
//
gulp.task( 'browser-sync:dev', function(){
    var config = {
        files: [
            paths.base.src.path + '**.html',
            paths.base.src.path + '**.css',
            paths.base.src.path + '**.js',
        ],
        proxy: {
            host: "localhost",
            port: 8888
        }
    };
    browserSync.init(config);
});
gulp.task( 'browser-sync:docs', function(){
    var config = {
        files: [
            paths.base.src.docs.path + '**.html',
            paths.base.src.docs.path + '**.css',
            paths.base.src.docs.path + '**.js',
        ],
        proxy: {
            host: "localhost",
            port: 8888
        }
    };
    browserSync.init(config);
});


//------------------------------------------------------
// TASKS
//------------------------------------------------------

// Clean Build
//
gulp.task( 'clean:build', function(callback){
    del(['build/**'], callback);
});


// JS Files: Build
//
gulp.task( 'scripts:distro', function(){
    return gulp.src( paths.src.js.path + '*.js' )
        .pipe( concat( 'tooth.min.js' ) )
        .pipe( uglify() )
        .pipe( header(getCopyright(), { version: getVersion(), date: getDate() } ) )
        .pipe( stripDebug() )
        .pipe( gulp.dest( paths.build.js.path ) );
});

gulp.task( 'scripts:dev', function(){
    return gulp.src( paths.src.js.path + '*.js' )
        .pipe( concat( 'tooth.js' ) )
        .pipe( header(getCopyright(), { version: getVersion(), date: getDate() } ) )
        .pipe( gulp.dest( paths.build.js.path ) );
});



//------------------------------------------------------
// WATCHERS
//------------------------------------------------------

// Dev: Watch file changes and compile files
//
gulp.task('watch:dev', ['browser-sync:dev']);

// Docs: Watch file changes and compile files
//
gulp.task('watch:docs', ['browser-sync:docs']);

//------------------------------------------------------
// BUILDS
//------------------------------------------------------

// default command: gulp
//
gulp.task("default", ['clean:build', 'scripts:distro', 'scripts:dev']);
